package com.kenzan.helloworldspringservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloworldSpringServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloworldSpringServiceApplication.class, args);
	}

}

