/*
 * Copyright 2018, kenzan Communications, All rights reserved.
 */
package com.kenzan.helloworldspringservice.api;


import com.kenzan.helloworldspringservice.service.HelloworldDomainService;
import com.kenzan.helloworldspringservice.swagger.mock.AvailableRescheduleResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;


@RestController
@RequestMapping ("helloworld/v1/")
public class V1ApiDelegateImpl {

    private static final Logger LOG = LoggerFactory.getLogger(V1ApiDelegateImpl.class);

    @Autowired
    private HelloworldDomainService domainService;

    @GetMapping ("availability")
    public ResponseEntity<AvailableRescheduleResponse> getRescheduleAvailability (String apptId, String xDomainSessionIdentity, HttpServletRequest httpServletRequest) {
        return domainService.getRescheduleAvailability(apptId, xDomainSessionIdentity);
    }

}
