/*
 * Copyright 2018, kenzan Communications,  All rights reserved.
 */
package com.kenzan.helloworldspringservice.command;

import com.kenzan.helloworldspringservice.exception.DomainResultCode;
import com.kenzan.helloworldspringservice.exception.DomainServiceException;
import com.kenzan.helloworldspringservice.mock.stuff.DomainCommand;
import com.kenzan.helloworldspringservice.mock.stuff.TransactionHelper;
import com.kenzan.helloworldspringservice.helpers.DateHelpers;
import com.kenzan.helloworldspringservice.helpers.EWOHelpers;
import com.kenzan.helloworldspringservice.mock.stuff.ScheduleWindow;
import com.kenzan.helloworldspringservice.requests.GetAvailableScheduleWindowsRequest;
import com.kenzan.helloworldspringservice.responses.GetAvailableScheduleWindowsResponse;
import com.kenzan.helloworldspringservice.service.EWOService;
import com.kenzan.helloworldspringservice.swagger.mock.AppointmentWindow;
import com.kenzan.helloworldspringservice.swagger.mock.AvailableRescheduleResponse;
import com.kenzan.helloworldspringservice.swagger.mock.HelloworldResultCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;


public class RescheduleAvailabilityEWOCommand extends DomainCommand<AvailableRescheduleResponse> {

    private static final Logger LOGGER = LoggerFactory.getLogger(RescheduleAvailabilityEWOCommand.class);

    private EWOService ewoService;
    private GetAvailableScheduleWindowsRequest request;

    public RescheduleAvailabilityEWOCommand (
            @NotNull EWOService ewoService,
            @NotNull GetAvailableScheduleWindowsRequest request) {
        super(GetAvailableScheduleWindowsRequest.class.getSimpleName(),30000);
        this.ewoService = ewoService;
        this.request = request;
    }

    @Override
    protected AvailableRescheduleResponse run () throws Exception {

        try {
            List<AppointmentWindow> availableApointments = new ArrayList<>();
            GetAvailableScheduleWindowsResponse responseAvailability = ewoService.getRescheduleAvailability(request);

            LOGGER.info("RescheduleAvailabilityEWOCommand - GetAvailableScheduleWindowsResponse: {}", responseAvailability);

            EWOHelpers.validationAvailableScheduleResponse(responseAvailability);

            List<ScheduleWindow> scheduleWindowsResponseList = responseAvailability.getReturn().getScheduleWindows();

            if(!scheduleWindowsResponseList.isEmpty()) {
                scheduleWindowsResponseList.forEach( scheduleWindow -> {

                    if(!scheduleWindow.getScheduleTimes().isEmpty()) {
                        scheduleWindow.getScheduleTimes().forEach(scheduleTime -> {

                            final AppointmentWindow.Builder appointmentWindowBuilder = new AppointmentWindow.Builder();
                            appointmentWindowBuilder.withStartDate(DateHelpers.asLocalDateFromXmlGregorianCalendar(scheduleWindow.getScheduleDate()));
                            appointmentWindowBuilder.withStartTime(scheduleTime.getTimeDescription());
                            appointmentWindowBuilder.withStartTimeCode(scheduleTime.getTimeCode());

                            availableApointments.add(appointmentWindowBuilder.build());
                        });
                    }
                });
            }

            return new AvailableRescheduleResponse.Builder()
                    .withAvailableWindows(availableApointments)
                    .withCode(parseServiceapptResultCode(responseAvailability.getReturn().getMessage()))
                    .withMessage(responseAvailability.getReturn().getMessage())
                    .withTimestamp(OffsetDateTime.now())
                    .withTxnId(TransactionHelper.getTransactionHeader())
                    .build();
        } catch (Exception e) {
            LOGGER.error("RescheduleAvailability failed in RescheduleAvailabilityEWOCommand due to: {}", e);
            throw new DomainServiceException("RescheduleAvailability failed in RescheduleAvailabilityEWOCommand", e, DomainResultCode.SERVER_ERROR);
        }

    }

    private HelloworldResultCode parseServiceapptResultCode(String codeName) {
        return HelloworldResultCode.fromValue(codeName);
    }
}
