/*
 * Copyright 2018, kenzan Communications,  All rights reserved.
 */
package com.kenzan.helloworldspringservice.dao;


import com.kenzan.helloworldspringservice.swagger.mock.AvailableRescheduleResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;

public interface EWODao {

    ResponseEntity<AvailableRescheduleResponse> getRescheduleAvailability (String apptId);
}
