/*
 * Copyright 2018, kenzan Communications,  All rights reserved.
 */
package com.kenzan.helloworldspringservice.dao.impl;


import com.kenzan.helloworldspringservice.command.RescheduleAvailabilityEWOCommand;
import com.kenzan.helloworldspringservice.helpers.DateHelpers;
import com.kenzan.helloworldspringservice.dao.EWODao;

import com.kenzan.helloworldspringservice.swagger.mock.AvailableRescheduleResponse;
import com.netflix.config.DynamicPropertyFactory;

import com.kenzan.helloworldspringservice.helpers.EWOHelpers;
import com.kenzan.helloworldspringservice.service.EWOService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.ZoneId;

@Component
public class EWODaoImpl implements EWODao {

    Logger LOGGER = LoggerFactory.getLogger(EWODaoImpl.class);

    @Value ("${helloworld.domain.mock.work_order_id}")
    private long WORK_ORDER_ID = (java.lang.Long)DynamicPropertyFactory.getInstance()
            .getLongProperty("helloworld.domain.mock.work_order_id", 12345).get();

    @Value ("${helloworld.domain.reschedule.availability.range.days:123}")
    private String RESCHEDULE_AVAILABILITY_RANGE_DAYS;

    @Value ("${helloworld.domain.timezone.zoneid}")
    private String TIME_ZONE_ID =
            DynamicPropertyFactory.getInstance().getStringProperty("helloworld.domain.timezone.zoneid", "US/Central").get();

    @Autowired
    EWOService ewoService;


    @Override
    public ResponseEntity<AvailableRescheduleResponse> getRescheduleAvailability(String apptId) {

        LOGGER.info("getRescheduleAvailability - apptId: {} and WORK_ORDER_ID: {}", apptId, WORK_ORDER_ID);

        // TODO: mocked apptId , remove it when we know is working
        Long apptIdMocked = WORK_ORDER_ID;

        LocalDate currentDate = LocalDate.now(ZoneId.of(TIME_ZONE_ID));
        LocalDate endDate = currentDate.plusDays(Integer.parseInt(RESCHEDULE_AVAILABILITY_RANGE_DAYS));

        return new RescheduleAvailabilityEWOCommand(ewoService,
                EWOHelpers.buildAvailableScheduleRequest(
                        apptIdMocked,
                        DateHelpers.asXMLGregorianCalendar(currentDate),
                        DateHelpers.asXMLGregorianCalendar(endDate)
                )).observe()
                .map((AvailableRescheduleResponse availableResponse) ->
                        ResponseEntity.ok(availableResponse))
                .toBlocking().single();
    }
}



