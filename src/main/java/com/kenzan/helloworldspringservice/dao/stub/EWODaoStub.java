/*
 * Copyright 2018, kenzan Communications,  All rights reserved.
 */
package com.kenzan.helloworldspringservice.dao.stub;


import com.kenzan.helloworldspringservice.dao.EWODao;
import com.kenzan.helloworldspringservice.mock.stuff.TransactionHelper;
import com.kenzan.helloworldspringservice.swagger.mock.AppointmentWindow;
import com.kenzan.helloworldspringservice.swagger.mock.AvailableRescheduleResponse;
import com.kenzan.helloworldspringservice.swagger.mock.HelloworldResultCode;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;



public class EWODaoStub implements EWODao {

    private static final String RESULT_MESSAGE = HelloworldResultCode.SUCCESS.toString();

    public static final String APPT_ID_FOR_EMPTY = "666";


    @Override
    public ResponseEntity<AvailableRescheduleResponse> getRescheduleAvailability (String apptId) {

        AppointmentWindow scheduleWindow = new AppointmentWindow.Builder()
                                                                .withStartDate(LocalDate.now())
                                                                .withStartTime("08:00 AM - 09:00 AM")
                                                                .build();

        List<AppointmentWindow> listAppointments =
                (!apptId.equals(APPT_ID_FOR_EMPTY))
                ? Arrays.asList(scheduleWindow)
                : new ArrayList<>();

        return ResponseEntity.ok(
                new AvailableRescheduleResponse.Builder()
                                               .withAvailableWindows(listAppointments)
                                               .withCode(HelloworldResultCode.SUCCESS)
                                               .withMessage("SUCCESS")
                                               .withTimestamp(OffsetDateTime.now())
                                               .withTxnId(TransactionHelper.getTransactionHeader())
                                               .build()
        );
    }

}
