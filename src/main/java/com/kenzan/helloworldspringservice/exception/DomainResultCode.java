package com.kenzan.helloworldspringservice.exception;

import java.util.Arrays;

public enum DomainResultCode {
    BAD_REQUEST(400),
    UNAUTHORIZED(401),
    NO_DATA_FOUND(404),
    PRECONDITION_FAILED(412),
    TOO_MANY_REQUESTS(429),
    SERVER_ERROR(500),
    NOT_IMPLEMENTED(501),
    SERVICE_UNAVAILABLE(503),
    SUCCESS(200),
    NO_CONTENT(204);

    private int status;

    private DomainResultCode(int status) {
        this.status = status;
    }

    public int status() {
        return this.status;
    }

    public static DomainResultCode fromValue(String value) {
        return (DomainResultCode)Arrays.stream(values()).filter((domainResultCode) -> {
            return domainResultCode.name().equalsIgnoreCase(value);
        }).findAny().orElse(SERVER_ERROR);
    }
}
