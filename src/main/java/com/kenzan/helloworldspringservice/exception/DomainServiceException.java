package com.kenzan.helloworldspringservice.exception;

public class DomainServiceException extends RuntimeException {
    private DomainResultCode domainResultCode;

    public DomainServiceException(String message, DomainResultCode domainResultCode) {
        super(message);
        this.domainResultCode = domainResultCode;
    }

    public DomainServiceException(String message, Throwable cause, DomainResultCode domainResultCode) {
        super(message, cause);
        this.domainResultCode = domainResultCode;
    }

    public DomainResultCode getDomainResultCode() {
        return this.domainResultCode;
    }
}
