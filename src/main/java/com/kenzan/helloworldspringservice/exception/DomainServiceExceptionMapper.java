package com.kenzan.helloworldspringservice.exception;

import java.time.OffsetDateTime;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import com.kenzan.helloworldspringservice.exception.ErrorResponse.Builder;
import com.kenzan.helloworldspringservice.mock.stuff.TransactionHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class DomainServiceExceptionMapper implements ExceptionMapper<Exception> {
    private static final Logger LOGGER = LoggerFactory.getLogger(DomainServiceExceptionMapper.class);

    public DomainServiceExceptionMapper() {
    }

    public Response toResponse(Exception ex) {
        LOGGER.error("Exception occurred: ", ex);
        Builder errorResponseBuilder = (new Builder()).withMessage(ex.getMessage()).withTimestamp(OffsetDateTime.now()).withTxnId(TransactionHelper.getTransactionHeader());
        if (ex instanceof DomainServiceException) {
            DomainServiceException domainServiceException = (DomainServiceException)ex;
            errorResponseBuilder.withCode(domainServiceException.getDomainResultCode().name()).withStatus(domainServiceException.getDomainResultCode().status());
        } else {
            errorResponseBuilder.withCode(DomainResultCode.SERVER_ERROR.name()).withStatus(DomainResultCode.SERVER_ERROR.status());
        }

        ErrorResponse errorResponse = errorResponseBuilder.build();
        return Response.status(errorResponse.getStatus()).entity(errorResponse).type(MediaType.APPLICATION_JSON_TYPE).build();
    }
}
