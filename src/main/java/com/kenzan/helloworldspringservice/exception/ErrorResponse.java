package com.kenzan.helloworldspringservice.exception;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.OffsetDateTime;

public class ErrorResponse {
    private final String txnId;
    private final OffsetDateTime timestamp;
    private final String message;
    private final String code;
    private final Integer status;

    private ErrorResponse(String txnId, OffsetDateTime timestamp, String message, String code, Integer status) {
        this.txnId = txnId;
        this.timestamp = timestamp;
        this.message = message;
        this.code = code;
        this.status = status;
    }

    @JsonProperty ("txnId")
    @JsonGetter
    public String getTxnId() {
        return this.txnId;
    }

    @JsonProperty("timestamp")
    @JsonGetter
    public OffsetDateTime getTimestamp() {
        return this.timestamp;
    }

    @JsonProperty("message")
    @JsonGetter
    public String getMessage() {
        return this.message;
    }

    @JsonProperty("code")
    @JsonGetter
    public String getCode() {
        return this.code;
    }

    @JsonProperty("status")
    @JsonGetter
    public Integer getStatus() {
        return this.status;
    }

    public static class Builder {
        private String txnId;
        private OffsetDateTime timestamp;
        private String message;
        private String code;
        private Integer status;

        public Builder() {
        }

        public ErrorResponse.Builder withTxnId(String txnId) {
            this.txnId = txnId;
            return this;
        }

        public ErrorResponse.Builder withTimestamp(OffsetDateTime timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public ErrorResponse.Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public ErrorResponse.Builder withCode(String code) {
            this.code = code;
            return this;
        }

        public ErrorResponse.Builder withStatus(Integer status) {
            this.status = status;
            return this;
        }

        public ErrorResponse build() {
            return new ErrorResponse(this.txnId, this.timestamp, this.message, this.code, this.status);
        }
    }
}