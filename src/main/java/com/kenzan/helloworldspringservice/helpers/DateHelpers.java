/*
 * Copyright 2018, kenzan Communications,  All rights reserved.
 */
package com.kenzan.helloworldspringservice.helpers;

import com.netflix.config.DynamicPropertyFactory;
import com.netflix.config.DynamicStringProperty;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateHelpers {

    private static final DynamicStringProperty TIME_ZONE_ID =
            DynamicPropertyFactory.getInstance().getStringProperty("helloworld.domain.timezone.zoneid", "US/Central");

    public static LocalDate asLocalDateFromXmlGregorianCalendar(XMLGregorianCalendar gregorianCalendar) {
        Date dateUtil = gregorianCalendar.toGregorianCalendar().getTime();

        return LocalDateTime.ofInstant(dateUtil.toInstant(), ZoneId.of(TIME_ZONE_ID.get())).toLocalDate();
    }

    public static XMLGregorianCalendar asXMLGregorianCalendar(LocalDate localDate) {
        GregorianCalendar calendar = new GregorianCalendar();
        Date utilDate = Date.from( localDate.atStartOfDay().atZone(ZoneId.of(TIME_ZONE_ID.get())).toInstant() );
        calendar.setTime(utilDate);
        try {
            return DatatypeFactory.newInstance()
                .newXMLGregorianCalendar(calendar);
        } catch (DatatypeConfigurationException e) {
            throw new RuntimeException(e);
        }
    }
}
