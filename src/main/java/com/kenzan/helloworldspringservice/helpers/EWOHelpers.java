package com.kenzan.helloworldspringservice.helpers;

import com.kenzan.helloworldspringservice.exception.DomainResultCode;
import com.kenzan.helloworldspringservice.exception.DomainServiceException;
import com.kenzan.helloworldspringservice.mock.stuff.ScheduleTime;
import com.kenzan.helloworldspringservice.mock.stuff.ScheduleWindow;
import com.kenzan.helloworldspringservice.mock.stuff.ScheduleWindows;
import com.kenzan.helloworldspringservice.requests.GetAvailableScheduleWindowsRequest;
import com.kenzan.helloworldspringservice.mock.stuff.StandardInputHeader;
import com.kenzan.helloworldspringservice.mock.stuff.TransactionHelper;
import com.kenzan.helloworldspringservice.responses.GetAvailableScheduleWindowsResponse;
import com.kenzan.helloworldspringservice.swagger.mock.HelloworldResultCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDate;

public class EWOHelpers {
    private static final Logger LOGGER = LoggerFactory.getLogger(EWOHelpers.class);

    private static final String TIME_CODE = "8AMTO9AM";

    public static GetAvailableScheduleWindowsRequest buildAvailableScheduleRequest (
            @NotNull final long workOrderId,
            @NotNull final XMLGregorianCalendar startDate,
            @NotNull final XMLGregorianCalendar endDate) {
        GetAvailableScheduleWindowsRequest request = new GetAvailableScheduleWindowsRequest();
        request.setWorkOrderId(workOrderId);
        request.setStartDate(startDate);
        request.setEndDate(endDate);
        return request;
    }

    public static void validationAvailableScheduleResponse(GetAvailableScheduleWindowsResponse response) {
        if (response.getReturn() == null) {
            LOGGER.error("AvailableReschedule failed in RescheduleAvailabilityEWOCommand due updateResponse.getReturn");
            throw new DomainServiceException("ResqueduleAvailability due to return null", DomainResultCode.SERVER_ERROR);
        }

        if (!response.getReturn().getErrors().isEmpty()) {
            LOGGER.error("AvailableReschedule failed in RescheduleAvailabilityEWOCommand due to message: {}",
                    response.getReturn().getMessage());
            throw new DomainServiceException("AvailableReschedule due to response status NOT SUCCESS", DomainResultCode.SERVER_ERROR);
        }

        LOGGER.info(" SUCCESS response in message: {}",
                response.getReturn().getMessage());

        if (!response.getReturn().getMessage().equals(HelloworldResultCode.SUCCESS.name())) {
            LOGGER.error("AvailableReschedule response with FAILURE in RescheduleAvailabilityEWOCommand due to message Code: {}",
                    response.getReturn().getMessage());
            throw new DomainServiceException("AvailableReschedule response with FAILURE in RescheduleAvailabilityEWOCommand", DomainResultCode.SERVER_ERROR);
        }
    }


    /**
     *  build mock of GetAvailableScheduleWindowsResponse
     * */
    public static GetAvailableScheduleWindowsResponse buildMockAvailableResponse () {
        GetAvailableScheduleWindowsResponse response;


        ScheduleWindow scheduleWindow = new ScheduleWindow();
        scheduleWindow.setScheduleDate(DateHelpers.asXMLGregorianCalendar(LocalDate.now()));

        ScheduleTime scheduleTime = new ScheduleTime();
        scheduleTime.setTimeCode(TIME_CODE);
        scheduleTime.setTimeDescription("08:00 AM - 09:00 AM");

        scheduleWindow.getScheduleTimes().add(scheduleTime);

        ScheduleWindows scheduleWindowList = new ScheduleWindows();
        scheduleWindowList.getScheduleWindows().add(scheduleWindow);

        response = new GetAvailableScheduleWindowsResponse();
        response.setReturn(scheduleWindowList);
        response.getReturn().setMessage("SUCCESS");
        return response;
    }

}
