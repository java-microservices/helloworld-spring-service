package com.kenzan.helloworldspringservice.helpers;

import com.kenzan.helloworldspringservice.mock.stuff.TransactionHelper;
import com.kenzan.helloworldspringservice.swagger.mock.CommonApiResponse;
import com.kenzan.helloworldspringservice.swagger.mock.HelloworldResponse;
import com.kenzan.helloworldspringservice.swagger.mock.HelloworldResultCode;
import com.kenzan.helloworldspringservice.swagger.mock.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Response;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * helpers to deal with common Helloworld Response munging
 */
public class HelloworldResponseHelpers {

    private static final Logger LOGGER = LoggerFactory.getLogger(HelloworldResponseHelpers.class);


    /**
     * @return
     * @implNote the TxnId field from the slf4j MDC -- will include a blank string in the message field if one isn't
     * supplied
     */
    public static CommonApiResponse getCommonApiResponse (Optional<String> messageOptional) {
        String message = messageOptional.orElse("");

        CommonApiResponse.Builder commonApiResponseBuilder =
                new CommonApiResponse.Builder()
                        .withTimestamp(OffsetDateTime.now())
                        .withTxnId(TransactionHelper.getTransactionHeader())
                        .withMessage(message);

        CommonApiResponse commonApiResponse = commonApiResponseBuilder.build();
        return commonApiResponse;
    }

    /**
     * @param messageOptional
     * @param responseStatus
     * @return
     */
    public static Response getResponseWithCommonApiResponse (Optional<String> messageOptional,
                                                             @NotNull Response.Status responseStatus) {

        CommonApiResponse commonApiResponse = getCommonApiResponse(messageOptional);

        Response populatedResponse = Response.status(responseStatus)
                .entity(commonApiResponse)
                .build();

        return populatedResponse;
    }

    /**
     * @param messageOptional
     * @param serviceapptsResultCode
     * @param orderListOptional
     * @return
     */
    public static HelloworldResponse getHelloworldResponse (Optional<String> messageOptional,
                                                            @NotNull HelloworldResultCode serviceapptsResultCode,
                                                            Optional<List<Order>> orderListOptional) {

        String message = messageOptional.orElse("");
        OffsetDateTime timeStamp = OffsetDateTime.now();
        String txnId = TransactionHelper.getTransactionHeader();
        List<Order> orderList = orderListOptional.orElse(new ArrayList<>());

        HelloworldResponse serviceapptsResponse = new HelloworldResponse.Builder()
                .withMessage(message)
                .withTimestamp(timeStamp)
                .withTxnId(txnId)
                .withCode(serviceapptsResultCode)
                .withOrders(orderList)
                .build();
        return serviceapptsResponse;
    }

}