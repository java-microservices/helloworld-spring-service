package com.kenzan.helloworldspringservice.mock.stuff;

import com.netflix.config.DynamicLongProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;

import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public abstract class DomainCommand<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(DomainCommand.class);
    private final String commandName;
    private final Supplier<T> fallback;
    private final DynamicLongProperty timeout;

    public DomainCommand(String commandName, Supplier<T> fallback, long defaultTimeout) {
        this.commandName = commandName;
        this.fallback = fallback;
        this.timeout = new DynamicLongProperty("domain.command." + commandName + ".timeout", defaultTimeout);
    }

    public DomainCommand(String commandName, long defaultTimeout) {
        this(commandName, (Supplier)null, defaultTimeout);
    }

    protected abstract T run() throws Exception;

    public T execute() {
        return this.observe().toBlocking().singleOrDefault(null);
    }

    public Observable<T> observe() {
        return Observable.fromCallable(new DomainCommand.Task());
    }

    private class Task implements Callable<T> {
        private Task() {
        }

        public T call() throws Exception {
            try {
                return DomainCommand.this.run();
            } catch (Exception var2) {
                DomainCommand.LOGGER.error("Error executing command. Command: " + DomainCommand.this.commandName, var2);
                if (Objects.isNull(DomainCommand.this.fallback)) {
                    throw var2;
                } else {
                    return DomainCommand.this.fallback.get();
                }
            }
        }
    }
}
