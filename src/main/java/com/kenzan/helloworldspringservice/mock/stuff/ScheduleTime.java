
package com.kenzan.helloworldspringservice.mock.stuff;

import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for scheduleTime complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="scheduleTime">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="timeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="timeDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "scheduleTime", propOrder = {
    "timeCode",
    "timeDescription"
})
public class ScheduleTime
    implements Equals, HashCode, ToString
{

    protected String timeCode;
    protected String timeDescription;

    /**
     * Gets the value of the timeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeCode() {
        return timeCode;
    }

    /**
     * Sets the value of the timeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeCode(String value) {
        this.timeCode = value;
    }

    /**
     * Gets the value of the timeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeDescription() {
        return timeDescription;
    }

    /**
     * Sets the value of the timeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeDescription(String value) {
        this.timeDescription = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theTimeCode;
            theTimeCode = this.getTimeCode();
            strategy.appendField(locator, this, "timeCode", buffer, theTimeCode);
        }
        {
            String theTimeDescription;
            theTimeDescription = this.getTimeDescription();
            strategy.appendField(locator, this, "timeDescription", buffer, theTimeDescription);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ScheduleTime)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ScheduleTime that = ((ScheduleTime) object);
        {
            String lhsTimeCode;
            lhsTimeCode = this.getTimeCode();
            String rhsTimeCode;
            rhsTimeCode = that.getTimeCode();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "timeCode", lhsTimeCode), LocatorUtils.property(thatLocator, "timeCode", rhsTimeCode), lhsTimeCode, rhsTimeCode)) {
                return false;
            }
        }
        {
            String lhsTimeDescription;
            lhsTimeDescription = this.getTimeDescription();
            String rhsTimeDescription;
            rhsTimeDescription = that.getTimeDescription();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "timeDescription", lhsTimeDescription), LocatorUtils.property(thatLocator, "timeDescription", rhsTimeDescription), lhsTimeDescription, rhsTimeDescription)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theTimeCode;
            theTimeCode = this.getTimeCode();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "timeCode", theTimeCode), currentHashCode, theTimeCode);
        }
        {
            String theTimeDescription;
            theTimeDescription = this.getTimeDescription();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "timeDescription", theTimeDescription), currentHashCode, theTimeDescription);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

}
