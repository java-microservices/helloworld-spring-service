
package com.kenzan.helloworldspringservice.mock.stuff;

import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for scheduleWindow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="scheduleWindow">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="scheduleDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="scheduleTimes" type="{http://www.kenzan.com/enterprise/workorder}scheduleTime" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "scheduleWindow", propOrder = {
    "scheduleDate",
    "scheduleTimes"
})
public class ScheduleWindow
    implements Equals, HashCode, ToString
{

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar scheduleDate;
    @XmlElement(nillable = true)
    protected List<ScheduleTime> scheduleTimes;

    /**
     * Gets the value of the scheduleDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getScheduleDate() {
        return scheduleDate;
    }

    /**
     * Sets the value of the scheduleDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setScheduleDate(XMLGregorianCalendar value) {
        this.scheduleDate = value;
    }

    /**
     * Gets the value of the scheduleTimes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the scheduleTimes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getScheduleTimes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ScheduleTime }
     * 
     * 
     */
    public List<ScheduleTime> getScheduleTimes() {
        if (scheduleTimes == null) {
            scheduleTimes = new ArrayList<ScheduleTime>();
        }
        return this.scheduleTimes;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            XMLGregorianCalendar theScheduleDate;
            theScheduleDate = this.getScheduleDate();
            strategy.appendField(locator, this, "scheduleDate", buffer, theScheduleDate);
        }
        {
            List<ScheduleTime> theScheduleTimes;
            theScheduleTimes = (((this.scheduleTimes!= null)&&(!this.scheduleTimes.isEmpty()))?this.getScheduleTimes():null);
            strategy.appendField(locator, this, "scheduleTimes", buffer, theScheduleTimes);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ScheduleWindow)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ScheduleWindow that = ((ScheduleWindow) object);
        {
            XMLGregorianCalendar lhsScheduleDate;
            lhsScheduleDate = this.getScheduleDate();
            XMLGregorianCalendar rhsScheduleDate;
            rhsScheduleDate = that.getScheduleDate();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "scheduleDate", lhsScheduleDate), LocatorUtils.property(thatLocator, "scheduleDate", rhsScheduleDate), lhsScheduleDate, rhsScheduleDate)) {
                return false;
            }
        }
        {
            List<ScheduleTime> lhsScheduleTimes;
            lhsScheduleTimes = (((this.scheduleTimes!= null)&&(!this.scheduleTimes.isEmpty()))?this.getScheduleTimes():null);
            List<ScheduleTime> rhsScheduleTimes;
            rhsScheduleTimes = (((that.scheduleTimes!= null)&&(!that.scheduleTimes.isEmpty()))?that.getScheduleTimes():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "scheduleTimes", lhsScheduleTimes), LocatorUtils.property(thatLocator, "scheduleTimes", rhsScheduleTimes), lhsScheduleTimes, rhsScheduleTimes)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            XMLGregorianCalendar theScheduleDate;
            theScheduleDate = this.getScheduleDate();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "scheduleDate", theScheduleDate), currentHashCode, theScheduleDate);
        }
        {
            List<ScheduleTime> theScheduleTimes;
            theScheduleTimes = (((this.scheduleTimes!= null)&&(!this.scheduleTimes.isEmpty()))?this.getScheduleTimes():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "scheduleTimes", theScheduleTimes), currentHashCode, theScheduleTimes);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

}
