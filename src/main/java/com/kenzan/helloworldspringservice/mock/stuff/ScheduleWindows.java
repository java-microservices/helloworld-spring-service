
package com.kenzan.helloworldspringservice.mock.stuff;

import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for scheduleWindows complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="scheduleWindows">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="autoSurveyFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="errors" type="{http://www.kenzan.com/enterprise/workorder}error" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="returnCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="scheduleWindows" type="{http://www.kenzan.com/enterprise/workorder}scheduleWindow" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="serviceDuration" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="timezone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "scheduleWindows", propOrder = {
    "autoSurveyFlag",
    "errors",
    "message",
    "returnCode",
    "scheduleWindows",
    "serviceDuration",
    "timezone"
})
public class ScheduleWindows
    implements Equals, HashCode, ToString
{

    protected boolean autoSurveyFlag;
    @XmlElement(nillable = true)
    protected List<Error> errors;
    protected String message;
    protected String returnCode;
    @XmlElement(nillable = true)
    protected List<ScheduleWindow> scheduleWindows;
    protected Long serviceDuration;
    protected String timezone;

    /**
     * Gets the value of the autoSurveyFlag property.
     * 
     */
    public boolean isAutoSurveyFlag() {
        return autoSurveyFlag;
    }

    /**
     * Sets the value of the autoSurveyFlag property.
     * 
     */
    public void setAutoSurveyFlag(boolean value) {
        this.autoSurveyFlag = value;
    }

    /**
     * Gets the value of the errors property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errors property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrors().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Error }
     * 
     * 
     */
    public List<Error> getErrors() {
        if (errors == null) {
            errors = new ArrayList<Error>();
        }
        return this.errors;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

    /**
     * Gets the value of the returnCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnCode() {
        return returnCode;
    }

    /**
     * Sets the value of the returnCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnCode(String value) {
        this.returnCode = value;
    }

    /**
     * Gets the value of the scheduleWindows property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the scheduleWindows property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getScheduleWindows().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ScheduleWindow }
     * 
     * 
     */
    public List<ScheduleWindow> getScheduleWindows() {
        if (scheduleWindows == null) {
            scheduleWindows = new ArrayList<ScheduleWindow>();
        }
        return this.scheduleWindows;
    }

    /**
     * Gets the value of the serviceDuration property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getServiceDuration() {
        return serviceDuration;
    }

    /**
     * Sets the value of the serviceDuration property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setServiceDuration(Long value) {
        this.serviceDuration = value;
    }

    /**
     * Gets the value of the timezone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimezone() {
        return timezone;
    }

    /**
     * Sets the value of the timezone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimezone(String value) {
        this.timezone = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            boolean theAutoSurveyFlag;
            theAutoSurveyFlag = this.isAutoSurveyFlag();
            strategy.appendField(locator, this, "autoSurveyFlag", buffer, theAutoSurveyFlag);
        }
        {
            List<Error> theErrors;
            theErrors = (((this.errors!= null)&&(!this.errors.isEmpty()))?this.getErrors():null);
            strategy.appendField(locator, this, "errors", buffer, theErrors);
        }
        {
            String theMessage;
            theMessage = this.getMessage();
            strategy.appendField(locator, this, "message", buffer, theMessage);
        }
        {
            String theReturnCode;
            theReturnCode = this.getReturnCode();
            strategy.appendField(locator, this, "returnCode", buffer, theReturnCode);
        }
        {
            List<ScheduleWindow> theScheduleWindows;
            theScheduleWindows = (((this.scheduleWindows!= null)&&(!this.scheduleWindows.isEmpty()))?this.getScheduleWindows():null);
            strategy.appendField(locator, this, "scheduleWindows", buffer, theScheduleWindows);
        }
        {
            Long theServiceDuration;
            theServiceDuration = this.getServiceDuration();
            strategy.appendField(locator, this, "serviceDuration", buffer, theServiceDuration);
        }
        {
            String theTimezone;
            theTimezone = this.getTimezone();
            strategy.appendField(locator, this, "timezone", buffer, theTimezone);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ScheduleWindows)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ScheduleWindows that = ((ScheduleWindows) object);
        {
            boolean lhsAutoSurveyFlag;
            lhsAutoSurveyFlag = this.isAutoSurveyFlag();
            boolean rhsAutoSurveyFlag;
            rhsAutoSurveyFlag = that.isAutoSurveyFlag();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "autoSurveyFlag", lhsAutoSurveyFlag), LocatorUtils.property(thatLocator, "autoSurveyFlag", rhsAutoSurveyFlag), lhsAutoSurveyFlag, rhsAutoSurveyFlag)) {
                return false;
            }
        }
        {
            List<Error> lhsErrors;
            lhsErrors = (((this.errors!= null)&&(!this.errors.isEmpty()))?this.getErrors():null);
            List<Error> rhsErrors;
            rhsErrors = (((that.errors!= null)&&(!that.errors.isEmpty()))?that.getErrors():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "errors", lhsErrors), LocatorUtils.property(thatLocator, "errors", rhsErrors), lhsErrors, rhsErrors)) {
                return false;
            }
        }
        {
            String lhsMessage;
            lhsMessage = this.getMessage();
            String rhsMessage;
            rhsMessage = that.getMessage();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "message", lhsMessage), LocatorUtils.property(thatLocator, "message", rhsMessage), lhsMessage, rhsMessage)) {
                return false;
            }
        }
        {
            String lhsReturnCode;
            lhsReturnCode = this.getReturnCode();
            String rhsReturnCode;
            rhsReturnCode = that.getReturnCode();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "returnCode", lhsReturnCode), LocatorUtils.property(thatLocator, "returnCode", rhsReturnCode), lhsReturnCode, rhsReturnCode)) {
                return false;
            }
        }
        {
            List<ScheduleWindow> lhsScheduleWindows;
            lhsScheduleWindows = (((this.scheduleWindows!= null)&&(!this.scheduleWindows.isEmpty()))?this.getScheduleWindows():null);
            List<ScheduleWindow> rhsScheduleWindows;
            rhsScheduleWindows = (((that.scheduleWindows!= null)&&(!that.scheduleWindows.isEmpty()))?that.getScheduleWindows():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "scheduleWindows", lhsScheduleWindows), LocatorUtils.property(thatLocator, "scheduleWindows", rhsScheduleWindows), lhsScheduleWindows, rhsScheduleWindows)) {
                return false;
            }
        }
        {
            Long lhsServiceDuration;
            lhsServiceDuration = this.getServiceDuration();
            Long rhsServiceDuration;
            rhsServiceDuration = that.getServiceDuration();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "serviceDuration", lhsServiceDuration), LocatorUtils.property(thatLocator, "serviceDuration", rhsServiceDuration), lhsServiceDuration, rhsServiceDuration)) {
                return false;
            }
        }
        {
            String lhsTimezone;
            lhsTimezone = this.getTimezone();
            String rhsTimezone;
            rhsTimezone = that.getTimezone();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "timezone", lhsTimezone), LocatorUtils.property(thatLocator, "timezone", rhsTimezone), lhsTimezone, rhsTimezone)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            boolean theAutoSurveyFlag;
            theAutoSurveyFlag = this.isAutoSurveyFlag();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "autoSurveyFlag", theAutoSurveyFlag), currentHashCode, theAutoSurveyFlag);
        }
        {
            List<Error> theErrors;
            theErrors = (((this.errors!= null)&&(!this.errors.isEmpty()))?this.getErrors():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "errors", theErrors), currentHashCode, theErrors);
        }
        {
            String theMessage;
            theMessage = this.getMessage();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "message", theMessage), currentHashCode, theMessage);
        }
        {
            String theReturnCode;
            theReturnCode = this.getReturnCode();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "returnCode", theReturnCode), currentHashCode, theReturnCode);
        }
        {
            List<ScheduleWindow> theScheduleWindows;
            theScheduleWindows = (((this.scheduleWindows!= null)&&(!this.scheduleWindows.isEmpty()))?this.getScheduleWindows():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "scheduleWindows", theScheduleWindows), currentHashCode, theScheduleWindows);
        }
        {
            Long theServiceDuration;
            theServiceDuration = this.getServiceDuration();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "serviceDuration", theServiceDuration), currentHashCode, theServiceDuration);
        }
        {
            String theTimezone;
            theTimezone = this.getTimezone();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "timezone", theTimezone), currentHashCode, theTimezone);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

}
