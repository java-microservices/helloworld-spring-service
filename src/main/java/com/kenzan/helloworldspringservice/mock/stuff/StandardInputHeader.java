
package com.kenzan.helloworldspringservice.mock.stuff;

import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for standardInputHeader complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="standardInputHeader">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="clientIp" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="clientHostName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="clientCallingService" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="auditUser" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="overrideCache" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "standardInputHeader", propOrder = {
    "clientIp",
    "clientHostName",
    "clientCallingService",
    "auditUser",
    "transactionId",
    "overrideCache"
})
public class StandardInputHeader
    implements Equals, HashCode, ToString
{

    @XmlElement(required = true)
    protected String clientIp;
    @XmlElement(required = true)
    protected String clientHostName;
    @XmlElement(required = true)
    protected String clientCallingService;
    @XmlElement(required = true)
    protected String auditUser;
    @XmlElement(required = true)
    protected String transactionId;
    protected Boolean overrideCache;

    /**
     * Gets the value of the clientIp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientIp() {
        return clientIp;
    }

    /**
     * Sets the value of the clientIp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientIp(String value) {
        this.clientIp = value;
    }

    /**
     * Gets the value of the clientHostName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientHostName() {
        return clientHostName;
    }

    /**
     * Sets the value of the clientHostName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientHostName(String value) {
        this.clientHostName = value;
    }

    /**
     * Gets the value of the clientCallingService property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientCallingService() {
        return clientCallingService;
    }

    /**
     * Sets the value of the clientCallingService property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientCallingService(String value) {
        this.clientCallingService = value;
    }

    /**
     * Gets the value of the auditUser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuditUser() {
        return auditUser;
    }

    /**
     * Sets the value of the auditUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuditUser(String value) {
        this.auditUser = value;
    }

    /**
     * Gets the value of the transactionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

    /**
     * Gets the value of the overrideCache property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOverrideCache() {
        return overrideCache;
    }

    /**
     * Sets the value of the overrideCache property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOverrideCache(Boolean value) {
        this.overrideCache = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theClientIp;
            theClientIp = this.getClientIp();
            strategy.appendField(locator, this, "clientIp", buffer, theClientIp);
        }
        {
            String theClientHostName;
            theClientHostName = this.getClientHostName();
            strategy.appendField(locator, this, "clientHostName", buffer, theClientHostName);
        }
        {
            String theClientCallingService;
            theClientCallingService = this.getClientCallingService();
            strategy.appendField(locator, this, "clientCallingService", buffer, theClientCallingService);
        }
        {
            String theAuditUser;
            theAuditUser = this.getAuditUser();
            strategy.appendField(locator, this, "auditUser", buffer, theAuditUser);
        }
        {
            String theTransactionId;
            theTransactionId = this.getTransactionId();
            strategy.appendField(locator, this, "transactionId", buffer, theTransactionId);
        }
        {
            Boolean theOverrideCache;
            theOverrideCache = this.isOverrideCache();
            strategy.appendField(locator, this, "overrideCache", buffer, theOverrideCache);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof StandardInputHeader)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final StandardInputHeader that = ((StandardInputHeader) object);
        {
            String lhsClientIp;
            lhsClientIp = this.getClientIp();
            String rhsClientIp;
            rhsClientIp = that.getClientIp();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "clientIp", lhsClientIp), LocatorUtils.property(thatLocator, "clientIp", rhsClientIp), lhsClientIp, rhsClientIp)) {
                return false;
            }
        }
        {
            String lhsClientHostName;
            lhsClientHostName = this.getClientHostName();
            String rhsClientHostName;
            rhsClientHostName = that.getClientHostName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "clientHostName", lhsClientHostName), LocatorUtils.property(thatLocator, "clientHostName", rhsClientHostName), lhsClientHostName, rhsClientHostName)) {
                return false;
            }
        }
        {
            String lhsClientCallingService;
            lhsClientCallingService = this.getClientCallingService();
            String rhsClientCallingService;
            rhsClientCallingService = that.getClientCallingService();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "clientCallingService", lhsClientCallingService), LocatorUtils.property(thatLocator, "clientCallingService", rhsClientCallingService), lhsClientCallingService, rhsClientCallingService)) {
                return false;
            }
        }
        {
            String lhsAuditUser;
            lhsAuditUser = this.getAuditUser();
            String rhsAuditUser;
            rhsAuditUser = that.getAuditUser();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "auditUser", lhsAuditUser), LocatorUtils.property(thatLocator, "auditUser", rhsAuditUser), lhsAuditUser, rhsAuditUser)) {
                return false;
            }
        }
        {
            String lhsTransactionId;
            lhsTransactionId = this.getTransactionId();
            String rhsTransactionId;
            rhsTransactionId = that.getTransactionId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "transactionId", lhsTransactionId), LocatorUtils.property(thatLocator, "transactionId", rhsTransactionId), lhsTransactionId, rhsTransactionId)) {
                return false;
            }
        }
        {
            Boolean lhsOverrideCache;
            lhsOverrideCache = this.isOverrideCache();
            Boolean rhsOverrideCache;
            rhsOverrideCache = that.isOverrideCache();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "overrideCache", lhsOverrideCache), LocatorUtils.property(thatLocator, "overrideCache", rhsOverrideCache), lhsOverrideCache, rhsOverrideCache)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theClientIp;
            theClientIp = this.getClientIp();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "clientIp", theClientIp), currentHashCode, theClientIp);
        }
        {
            String theClientHostName;
            theClientHostName = this.getClientHostName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "clientHostName", theClientHostName), currentHashCode, theClientHostName);
        }
        {
            String theClientCallingService;
            theClientCallingService = this.getClientCallingService();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "clientCallingService", theClientCallingService), currentHashCode, theClientCallingService);
        }
        {
            String theAuditUser;
            theAuditUser = this.getAuditUser();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "auditUser", theAuditUser), currentHashCode, theAuditUser);
        }
        {
            String theTransactionId;
            theTransactionId = this.getTransactionId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "transactionId", theTransactionId), currentHashCode, theTransactionId);
        }
        {
            Boolean theOverrideCache;
            theOverrideCache = this.isOverrideCache();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "overrideCache", theOverrideCache), currentHashCode, theOverrideCache);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

}
