package com.kenzan.helloworldspringservice.mock.stuff;

import org.slf4j.MDC;



public class TransactionHelper {

static final String MDC_TXNID = "MDC_TXNID";
static final String TRANSACTION_ID = "txnId";

    public static String getTransactionHeader() {
        return MDC.get(MDC_TXNID) != null ? MDC.get(MDC_TXNID) : MDC.get(TRANSACTION_ID);
    }

}
