package com.kenzan.helloworldspringservice.requests;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for getAvailableScheduleWindowsRequest complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="getAvailableScheduleWindowsRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="workOrderId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="orderSourceChannel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transferFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="startDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="endDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="operatorNTId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OPId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAvailableScheduleWindowsRequest", propOrder = {
        "workOrderId",
        "orderSourceChannel",
        "transferFlag",
        "startDate",
        "endDate",
        "operatorNTId",
        "opId"
})
public class GetAvailableScheduleWindowsRequest
        implements Equals, HashCode, ToString
{

    protected long workOrderId;
    protected String orderSourceChannel;
    protected String transferFlag;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startDate;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar endDate;
    protected String operatorNTId;
    @XmlElement(name = "OPId")
    protected String opId;

    /**
     * Gets the value of the workOrderId property.
     *
     */
    public long getWorkOrderId() {
        return workOrderId;
    }

    /**
     * Sets the value of the workOrderId property.
     *
     */
    public void setWorkOrderId(long value) {
        this.workOrderId = value;
    }

    /**
     * Gets the value of the orderSourceChannel property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOrderSourceChannel() {
        return orderSourceChannel;
    }

    /**
     * Sets the value of the orderSourceChannel property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOrderSourceChannel(String value) {
        this.orderSourceChannel = value;
    }

    /**
     * Gets the value of the transferFlag property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTransferFlag() {
        return transferFlag;
    }

    /**
     * Sets the value of the transferFlag property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTransferFlag(String value) {
        this.transferFlag = value;
    }

    /**
     * Gets the value of the startDate property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the operatorNTId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOperatorNTId() {
        return operatorNTId;
    }

    /**
     * Sets the value of the operatorNTId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOperatorNTId(String value) {
        this.operatorNTId = value;
    }

    /**
     * Gets the value of the opId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOPId() {
        return opId;
    }

    /**
     * Sets the value of the opId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOPId(String value) {
        this.opId = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            long theWorkOrderId;
            theWorkOrderId = this.getWorkOrderId();
            strategy.appendField(locator, this, "workOrderId", buffer, theWorkOrderId);
        }
        {
            String theOrderSourceChannel;
            theOrderSourceChannel = this.getOrderSourceChannel();
            strategy.appendField(locator, this, "orderSourceChannel", buffer, theOrderSourceChannel);
        }
        {
            String theTransferFlag;
            theTransferFlag = this.getTransferFlag();
            strategy.appendField(locator, this, "transferFlag", buffer, theTransferFlag);
        }
        {
            XMLGregorianCalendar theStartDate;
            theStartDate = this.getStartDate();
            strategy.appendField(locator, this, "startDate", buffer, theStartDate);
        }
        {
            XMLGregorianCalendar theEndDate;
            theEndDate = this.getEndDate();
            strategy.appendField(locator, this, "endDate", buffer, theEndDate);
        }
        {
            String theOperatorNTId;
            theOperatorNTId = this.getOperatorNTId();
            strategy.appendField(locator, this, "operatorNTId", buffer, theOperatorNTId);
        }
        {
            String theOPId;
            theOPId = this.getOPId();
            strategy.appendField(locator, this, "opId", buffer, theOPId);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof GetAvailableScheduleWindowsRequest)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final GetAvailableScheduleWindowsRequest that = ((GetAvailableScheduleWindowsRequest) object);
        {
            long lhsWorkOrderId;
            lhsWorkOrderId = this.getWorkOrderId();
            long rhsWorkOrderId;
            rhsWorkOrderId = that.getWorkOrderId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "workOrderId", lhsWorkOrderId), LocatorUtils.property(thatLocator, "workOrderId", rhsWorkOrderId), lhsWorkOrderId, rhsWorkOrderId)) {
                return false;
            }
        }
        {
            String lhsOrderSourceChannel;
            lhsOrderSourceChannel = this.getOrderSourceChannel();
            String rhsOrderSourceChannel;
            rhsOrderSourceChannel = that.getOrderSourceChannel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "orderSourceChannel", lhsOrderSourceChannel), LocatorUtils.property(thatLocator, "orderSourceChannel", rhsOrderSourceChannel), lhsOrderSourceChannel, rhsOrderSourceChannel)) {
                return false;
            }
        }
        {
            String lhsTransferFlag;
            lhsTransferFlag = this.getTransferFlag();
            String rhsTransferFlag;
            rhsTransferFlag = that.getTransferFlag();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "transferFlag", lhsTransferFlag), LocatorUtils.property(thatLocator, "transferFlag", rhsTransferFlag), lhsTransferFlag, rhsTransferFlag)) {
                return false;
            }
        }
        {
            XMLGregorianCalendar lhsStartDate;
            lhsStartDate = this.getStartDate();
            XMLGregorianCalendar rhsStartDate;
            rhsStartDate = that.getStartDate();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "startDate", lhsStartDate), LocatorUtils.property(thatLocator, "startDate", rhsStartDate), lhsStartDate, rhsStartDate)) {
                return false;
            }
        }
        {
            XMLGregorianCalendar lhsEndDate;
            lhsEndDate = this.getEndDate();
            XMLGregorianCalendar rhsEndDate;
            rhsEndDate = that.getEndDate();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "endDate", lhsEndDate), LocatorUtils.property(thatLocator, "endDate", rhsEndDate), lhsEndDate, rhsEndDate)) {
                return false;
            }
        }
        {
            String lhsOperatorNTId;
            lhsOperatorNTId = this.getOperatorNTId();
            String rhsOperatorNTId;
            rhsOperatorNTId = that.getOperatorNTId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "operatorNTId", lhsOperatorNTId), LocatorUtils.property(thatLocator, "operatorNTId", rhsOperatorNTId), lhsOperatorNTId, rhsOperatorNTId)) {
                return false;
            }
        }
        {
            String lhsOPId;
            lhsOPId = this.getOPId();
            String rhsOPId;
            rhsOPId = that.getOPId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "opId", lhsOPId), LocatorUtils.property(thatLocator, "opId", rhsOPId), lhsOPId, rhsOPId)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            long theWorkOrderId;
            theWorkOrderId = this.getWorkOrderId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "workOrderId", theWorkOrderId), currentHashCode, theWorkOrderId);
        }
        {
            String theOrderSourceChannel;
            theOrderSourceChannel = this.getOrderSourceChannel();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "orderSourceChannel", theOrderSourceChannel), currentHashCode, theOrderSourceChannel);
        }
        {
            String theTransferFlag;
            theTransferFlag = this.getTransferFlag();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "transferFlag", theTransferFlag), currentHashCode, theTransferFlag);
        }
        {
            XMLGregorianCalendar theStartDate;
            theStartDate = this.getStartDate();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "startDate", theStartDate), currentHashCode, theStartDate);
        }
        {
            XMLGregorianCalendar theEndDate;
            theEndDate = this.getEndDate();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "endDate", theEndDate), currentHashCode, theEndDate);
        }
        {
            String theOperatorNTId;
            theOperatorNTId = this.getOperatorNTId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "operatorNTId", theOperatorNTId), currentHashCode, theOperatorNTId);
        }
        {
            String theOPId;
            theOPId = this.getOPId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "opId", theOPId), currentHashCode, theOPId);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

}
