package com.kenzan.helloworldspringservice.responses;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.kenzan.helloworldspringservice.mock.stuff.ScheduleWindows;
import com.kenzan.helloworldspringservice.mock.stuff.StandardOutputHeader;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for getAvailableScheduleWindowsResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="getAvailableScheduleWindowsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://www.kenzan.com/enterprise/workorder}scheduleWindows" minOccurs="0"/>
 *         &lt;element name="kenzanHeaderOutputInfo" type="{http://www.kenzan.com/enterprise/workorder}standardOutputHeader" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAvailableScheduleWindowsResponse", propOrder = {
        "_return",
        "kenzanHeaderOutputInfo"
})
public class GetAvailableScheduleWindowsResponse
        implements Equals, HashCode, ToString
{

    @XmlElement(name = "return")
    protected ScheduleWindows _return;
    protected StandardOutputHeader kenzanHeaderOutputInfo;

    /**
     * Gets the value of the return property.
     *
     * @return
     *     possible object is
     *     {@link ScheduleWindows }
     *
     */
    public ScheduleWindows getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     *
     * @param value
     *     allowed object is
     *     {@link ScheduleWindows }
     *
     */
    public void setReturn(ScheduleWindows value) {
        this._return = value;
    }

    /**
     * Gets the value of the kenzanHeaderOutputInfo property.
     *
     * @return
     *     possible object is
     *     {@link StandardOutputHeader }
     *
     */
    public StandardOutputHeader getKenzanHeaderOutputInfo() {
        return kenzanHeaderOutputInfo;
    }

    /**
     * Sets the value of the kenzanHeaderOutputInfo property.
     *
     * @param value
     *     allowed object is
     *     {@link StandardOutputHeader }
     *
     */
    public void setKenzanHeaderOutputInfo(StandardOutputHeader value) {
        this.kenzanHeaderOutputInfo = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            ScheduleWindows theReturn;
            theReturn = this.getReturn();
            strategy.appendField(locator, this, "_return", buffer, theReturn);
        }
        {
            StandardOutputHeader theKenzanHeaderOutputInfo;
            theKenzanHeaderOutputInfo = this.getKenzanHeaderOutputInfo();
            strategy.appendField(locator, this, "kenzanHeaderOutputInfo", buffer, theKenzanHeaderOutputInfo);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof GetAvailableScheduleWindowsResponse)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final GetAvailableScheduleWindowsResponse that = ((GetAvailableScheduleWindowsResponse) object);
        {
            ScheduleWindows lhsReturn;
            lhsReturn = this.getReturn();
            ScheduleWindows rhsReturn;
            rhsReturn = that.getReturn();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "_return", lhsReturn), LocatorUtils.property(thatLocator, "_return", rhsReturn), lhsReturn, rhsReturn)) {
                return false;
            }
        }
        {
            StandardOutputHeader lhsKenzanHeaderOutputInfo;
            lhsKenzanHeaderOutputInfo = this.getKenzanHeaderOutputInfo();
            StandardOutputHeader rhsKenzanHeaderOutputInfo;
            rhsKenzanHeaderOutputInfo = that.getKenzanHeaderOutputInfo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "kenzanHeaderOutputInfo", lhsKenzanHeaderOutputInfo), LocatorUtils.property(thatLocator, "kenzanHeaderOutputInfo", rhsKenzanHeaderOutputInfo), lhsKenzanHeaderOutputInfo, rhsKenzanHeaderOutputInfo)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            ScheduleWindows theReturn;
            theReturn = this.getReturn();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "_return", theReturn), currentHashCode, theReturn);
        }
        {
            StandardOutputHeader theKenzanHeaderOutputInfo;
            theKenzanHeaderOutputInfo = this.getKenzanHeaderOutputInfo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "kenzanHeaderOutputInfo", theKenzanHeaderOutputInfo), currentHashCode, theKenzanHeaderOutputInfo);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

}
