package com.kenzan.helloworldspringservice.service;

import com.kenzan.helloworldspringservice.requests.GetAvailableScheduleWindowsRequest;
import com.kenzan.helloworldspringservice.responses.GetAvailableScheduleWindowsResponse;
import org.springframework.stereotype.Service;

public interface EWOService {

    /**
     *
     * @param request
     * @return
     */
    GetAvailableScheduleWindowsResponse getRescheduleAvailability (GetAvailableScheduleWindowsRequest request);

}