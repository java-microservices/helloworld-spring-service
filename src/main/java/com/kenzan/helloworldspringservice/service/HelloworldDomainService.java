/*
 * Copyright 2018, Kenzan Communications,  All rights reserved.
 */
package com.kenzan.helloworldspringservice.service;


import com.kenzan.helloworldspringservice.swagger.mock.AvailableRescheduleResponse;
import org.springframework.http.ResponseEntity;


public interface HelloworldDomainService {

    /**
     * Authenticates a given identity
     *
     * @param apptId
     * @param xDomainSessionIdentity
     * @return Response
     */
    ResponseEntity<AvailableRescheduleResponse> getRescheduleAvailability (String apptId, String xDomainSessionIdentity);



}
