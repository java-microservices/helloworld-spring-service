package com.kenzan.helloworldspringservice.service.impl;

import com.kenzan.helloworldspringservice.helpers.EWOHelpers;
import com.kenzan.helloworldspringservice.requests.GetAvailableScheduleWindowsRequest;
import com.kenzan.helloworldspringservice.responses.GetAvailableScheduleWindowsResponse;
import com.kenzan.helloworldspringservice.service.EWOService;
import org.springframework.stereotype.Component;

@Component
public class EWOServiceImpl implements EWOService {


    @Override
    public GetAvailableScheduleWindowsResponse getRescheduleAvailability (GetAvailableScheduleWindowsRequest request) {

        return EWOHelpers.buildMockAvailableResponse();
    }



}
