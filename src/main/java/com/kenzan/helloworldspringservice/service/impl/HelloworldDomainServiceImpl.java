/*
 * Copyright 2018, Kenzan Communications,  All rights reserved.
 */
package com.kenzan.helloworldspringservice.service.impl;


import com.kenzan.helloworldspringservice.dao.EWODao;
import com.kenzan.helloworldspringservice.service.HelloworldDomainService;

import com.kenzan.helloworldspringservice.swagger.mock.AvailableRescheduleResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;


/**
 * business logic layer for helloworld
 */
@Component
public class HelloworldDomainServiceImpl implements HelloworldDomainService {

    private static final Logger LOGGER = LoggerFactory.getLogger(HelloworldDomainServiceImpl.class);


    @Autowired
    private EWODao ewoDao;
    @Autowired
    private ObjectMapper objectMapper;
//    private final Decoder decoder;


    @Override
    public ResponseEntity<AvailableRescheduleResponse> getRescheduleAvailability(String apptId, String xDomainSessionIdentity) {

//        SessionIdentityInfo sessionIdentityInfo = decoder.decode(xDomainSessionIdentity, SessionIdentityInfo.class);

        ResponseEntity<AvailableRescheduleResponse> serviceResponse = ewoDao.getRescheduleAvailability(apptId);

        if (serviceResponse == null) {
            LOGGER.error("Failure in ewoImpl.getRescheduleAvailability response");
            return ResponseEntity.badRequest().build();
        }

        AvailableRescheduleResponse availableRescheduleResponse = serviceResponse.getBody();
        if(availableRescheduleResponse.getAvailableWindows().isEmpty() ) {
            return ResponseEntity.notFound().build();
        } else {
            return serviceResponse;
        }
    }


}
