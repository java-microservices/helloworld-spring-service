package com.kenzan.helloworldspringservice.api;

import com.fasterxml.jackson.databind.ObjectMapper;

import static org.junit.Assert.*;

import com.kenzan.helloworldspringservice.dao.EWODao;
import com.kenzan.helloworldspringservice.dao.stub.EWODaoStub;
import com.kenzan.helloworldspringservice.service.EWOService;
import com.kenzan.helloworldspringservice.service.impl.HelloworldDomainServiceImpl;
import com.kenzan.helloworldspringservice.swagger.mock.AppointmentWindow;
import com.kenzan.helloworldspringservice.swagger.mock.AvailableRescheduleResponse;
import com.kenzan.helloworldspringservice.swagger.mock.HelloworldResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.mockito.Mockito.*;
import org.mockito.Mock;
import org.mockito.Mockito;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ServiceApptsDomainServiceTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceApptsDomainServiceTest.class);

    private static final String TXNID_KEY = "txnId";
    private static final String TXNID_VALUE = "fake_txnId";
    private static final String ENCODED_IDENTITY_INFO = "eyJmb290cHJpbnQiOm51bGwsImNsaWVudElwQWRkcmVzcyI6IjE3Mi4wLjAuMSIsInNlc3Npb25JZCI6IjEyMzQ1IiwiYWNjb3VudEZvb3RwcmludE1hcCI6e30sICJhY2NvdW50TnVtYmVyIjoiODA4NzMwMDAyMDAzNjAwMyJ9";
    private static final String APPT_ID_DUMMY = "12345";

    @Mock
    private EWOService workOrderService;

    @MockBean
    private EWODao ewoDao;

    @Mock
    private ObjectMapper mapper;

    private EWODaoStub ewoDaoStub;
    @Autowired
    private HelloworldDomainServiceImpl helloworldService;

    private static final String TIME_CODE = "8AMTO9AM";
    private AppointmentWindow appointmentWindow;


    @Before
    public void init() {
        // MDC = Mapped Diagnostic Context
        // https://logback.qos.ch/manual/mdc.html
        MDC.put(TXNID_KEY, TXNID_VALUE);

        ewoDaoStub = new EWODaoStub();

        appointmentWindow = new AppointmentWindow.Builder()
                .withStartDate(LocalDate.now())
                .withStartTime(TIME_CODE)
                .build();

    }

    @Test
    public void getRescheduleAvailabilityTest() {

        when(ewoDao.getRescheduleAvailability(Mockito.any()))
                .thenReturn(ewoDaoStub.getRescheduleAvailability(APPT_ID_DUMMY));

        ResponseEntity<AvailableRescheduleResponse> response =
                helloworldService.getRescheduleAvailability(APPT_ID_DUMMY, ENCODED_IDENTITY_INFO);

        assertNotNull(response.getBody());
        assertTrue(response.getBody() instanceof HelloworldResponse);

        AvailableRescheduleResponse availableRescheduleResponse = response.getBody();

        assertEquals("SUCCESS", availableRescheduleResponse.getMessage());
        assertEquals(1, availableRescheduleResponse.getAvailableWindows().size());
    }

    @Test(expected = Exception.class)
    public void getRescheduleAvailabilityExceptionTest() throws Exception {

        when(workOrderService.getRescheduleAvailability(Mockito.any()))
                .thenThrow(new Exception("SAMPLE_ERROR"));

        helloworldService.getRescheduleAvailability(APPT_ID_DUMMY, ENCODED_IDENTITY_INFO);
    }


    @Test
    public void getRescheduleAvailabilityIsEmptyTest() {
        String apptId = EWODaoStub.APPT_ID_FOR_EMPTY;

        when(ewoDao.getRescheduleAvailability(Mockito.any()))
                .thenReturn(ewoDaoStub.getRescheduleAvailability(apptId));

        ResponseEntity<AvailableRescheduleResponse> response = helloworldService.getRescheduleAvailability(APPT_ID_DUMMY, ENCODED_IDENTITY_INFO);

        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }
}

